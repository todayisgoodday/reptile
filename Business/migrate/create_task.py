# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/12/12 09:59
# File      : create_task.py
# explain   : 文件说明

from common.http_api import Api
from business.migrate.login import Login
from common.read_yaml import get_yml_data, revise_yml_data
from common.migrate_source import Replace


class CreateTask(Api):

    def __init__(self):
        super().__init__(host='192.168.3.127:82')
        self.headers = {
            'Content-Type': 'application/json;charset=UTF-8',
            'Accept': 'application/json, text/plain, */*'
        }
        self.login = Login().default_login()
        migrate_config = get_yml_data('migrate', 'config', 'default')
        migrate_groupname = get_yml_data('migrate', 'groupname', 'group')

        self.group_name = migrate_groupname[0]['palayod']['groupName']
        self.save_task_name = migrate_config['task_name']
        self.schema = migrate_config['schema']

        token_info = get_yml_data('migrate', 'migrate', 'token')[0]['palayod']
        self.headers.update({"authorization": token_info['Authorization']})
        self.headers.update({"Cookie": token_info['Cookie']})

    def get_task(self):
        """
        获取迁移分组列表
        :return:
        """
        url = '/migra/group/list?_t=1702370663619'
        response = self.request('get', url, headers=self.headers).json()
        group_id = next((group['groupId'] for group in response['data'] if group['groupName'] == self.group_name), None)
        if group_id:
            revise_yml_data('migrate', 'config', 'default', 'group_id', int(group_id))
        return group_id

    def create_task(self, payload):
        """
        srcCgj 3 oracle-203  。 tgtCgj oracle-158   type 0 迁移全部  10 仅迁移对象  20 仅迁移数据
        :param payload:
        :return:
        """
        url = '/migra/task/add'
        return self.request('put', url, payload=payload, headers=self.headers)

    def task_list(self, payload):
        """
        从 get_task 获取 group_id  需要传入字典
        :return:
        """
        url = '/migra/task/taskList'
        return self.request('post', url, payload=payload, headers=self.headers)

    def task_id(self, payload):
        """
        从 task_list 中获取
        :param payload:
        :return:
        """
        # 获取任务id方便后面配置表或对象
        for task in payload['data']['records']:
            if self.save_task_name == task['taskName']:
                return task['taskId']

    def src_tables(self, payload):
        """
        获取源端选择 schema 下的所有表，组合好之后返回
        :param response:
        :return:
        """
        task_id = payload
        schemas = None
        schema_table_list = []

        if task_id:
            url = '/migra/obj/getSchema?taskId=%s&_t=1702518394543' % task_id
            schema_list = self.request('get', url, headers=self.headers).json().get('data', [])
            schemas = next((schema for schema in schema_list if self.schema == schema), None)

            if schemas:
                get_all_schema = '/migra/obj/getObjBySchema'
                rep = {
                    "taskId": task_id,
                    "schema": schemas,
                    "type": 0,
                    "selectType": 0
                }

                choice_schema = self.request('post', get_all_schema, payload=rep, headers=self.headers)
                schema_table_list = choice_schema.json().get('data', [])

        check_list = []
        for table in schema_table_list:
            model = {
                "fieldList": [],
                "taskId": int(task_id),
                "srcSchema": schemas,
                "tgtSchema": schemas,
                "srcObj": table,
                "tgtObj": table,
                "type": 0,
                "order": 1
            }

            check_list.append(model)

        return check_list

    def check_tgr(self, payload):
        """
        检查目标端是否存在该表
        :param payload:
        :return:
        """
        url = '/migra/obj/checkTgtObj'

        return self.request('post', url, payload, headers=self.headers)

    def del_tgr(self, payload):
        """
        删除delete
        :param payload:
        :return:
        """
        url = '/migra/obj/delBatchTgtObj'

        return self.request('post', url, payload, headers=self.headers)


if __name__ == '__main__':
    api = CreateTask()
    group = {"groupId": "55", "current": 1, "pageSize": 10}
    group_id = api.task_list(group).json()
    task_id = api.task_id(group_id)
    id = 1734873428869050370

    src_list = api.check_tgr(api.src_tables(id)).json()['data']['objList']
    print(src_list)

    # print(api.src_tables(id))
    # print(api.del_tgr(src_list))
    # print(api.del_tgr(src_list).json())
