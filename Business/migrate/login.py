# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/12/12 09:59
# File      : login.py
# explain   : 文件说明
import time

from common.http_api import Api
from common.read_yaml import get_yml_data, revise_yml_data


class Login(Api):

    def __init__(self):
        super(Login, self).__init__(host='192.168.3.127:82')
        self.headers = {"Content-Type": "application/json;"}

    def login(self, payload):
        url = '/login'
        return self.request('get', url, payload)

    def default_login(self):
        url = '/login'
        login = get_yml_data('migrate', 'login', 'login')[0]['palayod']
        au_time = get_yml_data('migrate', 'migrate', 'token')[0]['palayod']['Times']

        response = self.request('get', url, login)

        revise_yml_data('migrate', 'migrate', 'token', 'palayod', response.json()['data'], 'Authorization')
        revise_yml_data('migrate', 'migrate', 'token', 'palayod', response.headers['Set-Cookie'].split(';')[0], 'Cookie')
        revise_yml_data('migrate', 'migrate', 'token', 'palayod', int(time.time()), 'Times')


if __name__ == '__main__':
    param = {
        "username": "admin",
        "password": "Great@2023",
        "_t": "1702348256170"
    }
    info = get_yml_data('migrate', 'login', 'login')[0]['palayod']
    print(info)
