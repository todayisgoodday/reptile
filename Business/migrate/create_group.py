# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/12/12 09:59
# File      : create_group.py
# explain   : 文件说明
import requests

from common.http_api import Api
from business.migrate.login import Login
from common.read_yaml import get_yml_data


class CreateGroup(Api):

    def __init__(self):
        super().__init__(host='192.168.3.127:82')

        self.headers = {
            'Content-Type': 'application/json;charset=UTF-8',
            'Accept': 'application/json, text/plain, */*',
            'User-Agent':
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36",
            'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7'
        }
        self.login = Login()

    def Creatgroup(self, payload):
        """
        创建迁移分组
        :param token:
        :param name:
        :return:
        """

        url = '/migra/group/add'
        self.login.default_login()
        self.headers.update(
            {"authorization": get_yml_data('migrate', 'migrate', 'token')[0]['palayod']['Authorization']})
        self.headers.update({"Cookie": get_yml_data('migrate', 'migrate', 'token')[0]['palayod']['Cookie']})

        return self.request('put', url, payload=payload, headers=self.headers)


if __name__ == '__main__':
    print(CreateGroup().Creatgroup(payload=get_yml_data('migrate', 'groupname', 'group')[0]['palayod']).json())
    # print(CreateGroup().Creatgroup(get_yml_data('migrate', 'groupname', 'group')[0]['palayod']))