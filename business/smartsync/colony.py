# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/8/15 15:40
# File      : colony.py
# explain   : 文件说明


import requests
from common.read_yaml import get_yml_data, revise_yml_data
from common.log import logger
from common.base import Bases


class Colony(Bases):

    def __init__(self):
        super().__init__()

    def CreateColony(self, colo_name=None):
        # 创建请求
        if colo_name:
            revise_yml_data('sync','colony', 'colony', 'name', colo_name)

        res = super().Post(url=get_yml_data('sync','all_param', 'urls')['create_colony'],
                           headers=get_yml_data('sync','all_param', 'headers'),
                           json=get_yml_data('sync','colony', 'colony'))

        return res.json()


if __name__ == '__main__':
    info = {
        "name": "229",
        "remark": "229",
        "members": [
            {
                "name": "192.168.3.198",
                "ip": "192.168.3.198",
                "port": "8083",
                "mport": "8085",
                "remark": "这是服务描述哦"
            }
        ]
    }
    res = Colony()
    res.CreateColony('akkks')
