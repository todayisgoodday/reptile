# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/8/14 10:32
# File      : Logins.py
# explain   : 文件说明

import json

import requests
# from simple_settings import settings
from common.http_api import Api
from common.read_yaml import get_yml_data, revise_yml_data
from common.log import logger
from common.base import Bases


class Login(Api):

    def __init__(self):
        super(Login, self).__init__(host='192.168.3.127:8081')
        # super(Login, self).__init__(host=settings.api_host)
        self.headers = {"Content-Type": "application/json;"}

    def login(self, payload):
        """
        底层只维护接口信息
        :param payload:
        :param token:
        :return:
        """
        url = '/login'
        Bases().GetCode()
        return self.request('post', url, payload)
