# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/12/12 17:44
# File      : migrate_source.py
# explain   : 文件说明

import time
from common.read_yaml import get_yml_data, revise_yml_data


class Replace:

    def __init__(self):
        self.db_type = get_yml_data('migrate', 'config', 'default')['database_type']
        self.migrate_type = get_yml_data('migrate', 'config', 'default')['migrate_type']
        self.task_name = int(round(time.time() * 1000)) % 10000

        self.group_id = get_yml_data('migrate', 'config', 'default')['group_id']
        self.generated_task_name = None  # 初始化一个存储生成的 taskName 的变量

    def replace(self):

        source_config = {
            'oracle': self.oracle,
            'mysql': self.mysql,
            'dm': {'db_type': self.mysql}
        }

        # 生成 taskName 并存储到 generated_task_name 变量中
        self.generated_task_name = "all-{}-{:04d}".format(self.db_type, self.task_name)
        # 存储task_name 方便查询task_id
        revise_yml_data('migrate', 'config', 'default', 'task_name', self.generated_task_name)
        return source_config[self.db_type](self.migrate_type)

    def oracle(self, type=None):
        oracle_configs = {
            "all": {
                "groupId": self.group_id,  # 分组id
                "taskName": self.generated_task_name,
                "type": 0,  # type 0 迁移全部  10 仅迁移对象  20 仅迁移数据
                "srcConfigId": 10,  # 源端
                "tgtConfigId": 4,  # 目标端
                "enableLog": "Y",  # Y 是开启  N是关闭
                "enableScn": "Y",  # Y 是开启  N是关闭
            },
            "data": {
                "groupId": self.group_id,
                "taskName": self.generated_task_name,
                "type": 20,
                "srcConfigId": 10,
                "tgtConfigId": 4,
                "enableLog": "Y",
                "enableScn": "Y",
            },
            "obj": {
                "groupId": self.group_id,
                "taskName": self.generated_task_name,
                "type": 10,
                "srcConfigId": 10,
                "tgtConfigId": 4,
                "enableLog": "N",
                "enableScn": "N",
            }
        }

        if type in oracle_configs:
            return oracle_configs[type]
        else:
            return 'fail type'

    def mysql(self, type=None):
        mysql_configs = {
            "all": {
                "groupId": self.group_id,  # 分组id
                "taskName": self.generated_task_name,
                "type": 0,  # type 0 迁移全部  10 仅迁移对象  20 仅迁移数据
                "srcConfigId": 23,  # 源端
                "tgtConfigId": 24,  # 目标端
                "enableLog": "Y",  # Y 是开启  N是关闭
                "enableScn": "Y",  # Y 是开启  N是关闭
            },
            "data": {
                "groupId": self.group_id,
                "taskName": self.generated_task_name,
                "type": 20,
                "srcConfigId": 23,
                "tgtConfigId": 24,
                "enableLog": "Y",
                "enableScn": "Y",
            },
            "obj": {
                "groupId": self.group_id,
                "taskName": self.generated_task_name,
                "type": 10,
                "srcConfigId": 23,
                "tgtConfigId": 24,
                "enableLog": "N",
                "enableScn": "N",
            }
        }

        if type in mysql_configs:
            return mysql_configs[type]
        else:
            return 'fail type'


if __name__ == '__main__':
    res = Replace()
    print(res.replace())
