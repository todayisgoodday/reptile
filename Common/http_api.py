# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/9/7 16:06
# File      : http_api.py
# explain   : 文件说明
import json

import requests

from common.request import HttpRequest


class Api(HttpRequest):
    """
    生产、测试环境分离
    """

    def __init__(self, host):
        self.host = host
        self.proto = 'http'

    def request(self, method, url, payload=None, headers=None, data=None, **kwargs):

        domain = f"{self.proto}://{self.host}{url}"

        if method == "get":
            resp = self.get(domain, params=payload, headers=headers, **kwargs)
        elif method == "put":  # put 方法也是用json，需要在底层方法中添加json
            resp = self.put(domain, json=payload, headers=headers,**kwargs)
        else:
            resp = self.post(domain, json=payload, headers=headers, **kwargs)
        assert resp.status_code == 200, url + "接口访问异常"
        return resp


if __name__ == '__main__':
    # params = {
    #     "username": "admin",
    #     "password": "Great@2023",
    #     "_t": "1702348256170"
    # }
    #
    # response = requests.get(url='http://192.168.3.127:82/login', params=params)
    #
    # if response.status_code == 200:
    #     try:
    #         json_data = response.json()
    #         print("JSON 数据:", json_data)
    #     except Exception as e:
    #         print("无法解析为 JSON 格式:", e)
    #         print("实际返回内容:", response.text)
    # else:
    #     print("请求失败，状态码:", response.status_code)
    #     print("错误信息:", response.text)

    pal = {
        "groupName": "lishouwu-Test13"
    }


    headers = {'Content-Type': 'application/json;charset=UTF-8', 'Accept': 'application/json, text/plain, */*',
               'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36',
               'Accept-Language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
               'authorization': 'a54034ae-26d5-4143-bb3a-2ddbb24c6598',
               'Cookie': 'SESSION=MDEyMTdjYWQtNWFmZi00NTE3LTlmYTgtNDI2YmNmYjFlM2U5'}

    api = Api('192.168.3.127:82')
    response = api.request('put','/migra/group/add',payload=pal,headers=headers)
    print(response.json())
