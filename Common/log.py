# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/8/15 11:03
# File      : log.py
# explain   : 文件说明

from common.read_yaml import get_yml_data
from nb_log import LogManager


class LogsUtils:

    def logger(self):
        logger = LogManager('Api_Test').get_logger_and_add_handlers(
            is_add_stream_handler=True,
            log_filename=get_yml_data('config','all_param', 'default')['log_name']
        )
        return logger


logger = LogsUtils().logger()

