# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/8/14 14:06
# File      : read_yaml.py
# explain   : 文件说明


import yaml
from yaml import Loader
import os
import pathlib


def get_yml_data(file_path, file_name, data_name=None):
    """
    module  yml文件所在模块下
    """
    base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    data_file_path = os.path.join(base_path, f'testcase/{file_path}', f'{file_name}.yaml')
    file_data = pathlib.Path(data_file_path).read_text()

    return (
        yaml.load(file_data, Loader=Loader).get(data_name)
        if data_name
        else yaml.load(file_data, Loader=Loader, )
    )


def revise_yml_data(file_path, file_name, dict_key, second_key, info_data, end_key=None):
    """
    :param file_path:  testcase下的文件目录
    :param file_name:  yaml文件名字
    :param dict_key:  第一个字典名称
    :param second_key:  字典下的第二字典名称
    :param info_data: 替换字典的key
    :param end_key:  替换字典的values
    :return:
    """
    base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    data_file_path = os.path.join(base_path, f'testcase/{file_path}/', f'{file_name}.yaml')
    # data_file_path = os.path.join(base_path, f'Data/sync/', f'{file_name}.yaml')

    with open(data_file_path, 'r', encoding='utf-8') as file:
        data = yaml.safe_load(file)
        file.close()

    with open(data_file_path, 'w', encoding='utf-8') as file:
        if end_key:
            data[dict_key][0][second_key][end_key] = info_data
            yaml.dump(data, file, allow_unicode=True)
            file.close()
        else:
            data[dict_key][second_key] = info_data
            yaml.dump(data, file, allow_unicode=True)
            file.close()


if __name__ == '__main__':
    # print(get_yml_data('sync', 'login'))
    # print(get_yml_data('sync', 'login', 'login_params'))
    # print(get_yml_data('migrate', 'migrate', 'token'))
    # print(get_yml_data('migrate', 'creatgroup', 'creatgroup')[0]['palayod'])
    # print(get_yml_data('sync', 'login', 'login_params')[0]['palayod'])
    # print(get_yml_data('migrate', 'create_task', 'task_list'))
    # revise_yml_data('config', 'source', 'source', 'palayod', 'asdasdsd','name')
    # print(get_yml_data('config', 'source','source_198'))
    # print(get_yml_data('config', 'token','token'))
    # revise_yml_data('config', 'token', 'token', 'palayod', 'dasdasdsd','Authorization')
    # revise_yml_data('config', 'source', 'source_198', 'palayod', 'asdmasdasd112123','name')
    # print(get_yml_data('config', 'source', 'source'))
    # print(get_yml_data('sync', 'login', 'login_params')[0]['palayod'])
    revise_yml_data('migrate', 'config', 'default', 'group_id', 50)
