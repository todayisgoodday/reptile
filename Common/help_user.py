# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/9/19 16:09
# File      : help_user.py
# explain   : 文件说明

from time import time
from common.base import Bases
from common.log import logger
from common.http_api import Api
from business.smartsync.login import Login
from common.read_yaml import get_yml_data, revise_yml_data


def GetToken():
    """
    前置获取token，提供token给其他接口使用
    :return:
    """
    max_retries = 3  # Maximum number of retries
    retry_count = 0

    while retry_count < max_retries:
        try:
            token_info = get_yml_data('config', 'token', 'token')[0]['palayod']
            current_time = token_info['current_time']
            if time() - current_time < 1800:
                logger.info("token未超时，进来拿token咯")
                return token_info['Authorization']

            response = Login().login(get_yml_data('sync', 'login', 'login_params')[0]['palayod'])
            print("zzzzzzzzzz  response : " ,response.json())
            new_token = response.json()['data']['Authorization']
            revise_yml_data('config', 'token', 'token', 'palayod', new_token, 'Authorization')
            revise_yml_data('config', 'token', 'token', 'palayod', int(time()), 'current_time')
            logger.info("token超时，重新拿token咯")
            return new_token

        except TypeError:
            logger.error("尝试获取或刷新token时发生异常。重试...")
            retry_count += 1

    logger.error("重试 {} 后无法获取token信息.".format(max_retries))
    return None  # Return None or handle the failure according to your requirements


if __name__ == '__main__':
    print(GetToken())
