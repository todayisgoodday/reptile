# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/8/15 16:24
# File      : create_connect.py
# explain   : 文件说明

from simple_settings import settings

from common.base import Bases
from common.http_api import Api
from common.help_user import GetToken


class CreateSync(Api):

    def __init__(self):
        super().__init__(host=settings.api_host)
        # super().__init__(host='192.168.3.198:8081')
        self.bases = Bases()
        self.headers = {'Content-Type': 'application/json;', 'Accept': 'application/json, text/plain, */*'}

    def CreateSource(self, token, name=None):
        url = 'gssConfig/submitList'
        self.headers.update({"Authorization": token})
        return self.request('post', url, payload=self.bases.ReplaceSource(name), headers=self.headers)


if __name__ == '__main__':
    token = GetToken()
    print(CreateSync().CreateSource(token, '2002年').json())
    # print(CreateSync().CreateSink(token, 'Auto-Request0922-a1').json())
