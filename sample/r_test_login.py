# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/9/7 17:37
# File      : login.py
# explain   : 文件说明


import json
import pytest
from common.read_yaml import get_yml_data
from business.smartsync.login import Login
from common.log import logger
from common.assetcase import TestCase
from common.base import Bases


@pytest.mark.usefixtures('user_fixture')
class TestLogin(TestCase):

    @classmethod
    def setup_class(cls):
        logger.debug("类前置调用")
        cls.action = Login()

    @pytest.mark.parametrize('case_data', get_yml_data('sync', 'login', 'login_params'))
    def test_login(self, user_fixture, case_data):
        response = Login().login(user_fixture)
        print(response.json())
        self.assertJson(case_data.get('verify')), '登录失败，测试用例报错......'


if __name__ == '__main__':
    pytest.main(['-s', '-v', 'r_test_login.py'])
