# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/8/14 09:13
# File      : get_token.py
# explain   : 文件说明
import base64

import requests
import json
import ddddocr

ocr = ddddocr.DdddOcr()

res = requests.get(url='http://192.168.3.198:8081/auth/captcha?_t=1691975640316')
print(res.json())
print(res.json()['data']['img'])

info = res.json()['data']['img']
ss = json.dumps(info)
es = ss.split('base64,')

file = open('./sx.png', 'wb')
file.write(base64.b64decode(es[1]))
file.close()


def get_captcha(screen_dir='./sx.png'):
    with open(screen_dir, "rb") as pf:
        img_bytes = pf.read()
    ss = ocr.classification(img_bytes)
    return ss


print(get_captcha())
