# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/8/11 14:21
# File      : main.py
# explain   : 文件说明

from common.base import Bases
from business.smartsync.login import Login
from business.smartsync.create_connect import CreateSync


class Main:
    """
    同步main方法  后续需要整合或者拆分开
    """

    def __init__(self):
        self.bases = Bases()
        self.login = Login()

    def CreatConnect(self):
        """
        创建连接器
        :return:
        """
        pass


if __name__ == '__main__':
    Main()
    # print(mai.CreateSyncConnect('198', 'source', '0822-TestOnline-z'))
    # print(mai.CreateSyncConnect('198', 'source', '0825-TOnline-z-source'))
