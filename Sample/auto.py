# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/12/14 14:53
# File      : auto.py
# explain   : 文件说明

str_list = []
for i in range(5):
    str_num = 'num' + str(i + 1)
    str_list.append(str_num)

print(str_list)
# 传两个参数
it = iter(str_list)
for _ in range(8):
    # res = next(it, -1)
    res = next(it, None)
    print(res)

