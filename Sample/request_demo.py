# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/11/2 09:52
# File      : request_demo.py
# explain   : 文件说明


import requests

# 设置搜索关键词
search_query = "芜湖"

# 构建搜索URL
search_url = f"http://www.baidu.com/s?wd=kknkn"

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36'}

"""
甲乙 木
丙丁 火
戊己 土
庚辛 金
壬葵 水


"""

# 发起GET请求
response = requests.get(search_url, headers=headers)

# 检查响应状态码
if response.status_code == 200:
    # 显示搜索结果页面内容
    print("搜索结果页面内容:")
    print(response.content.decode('utf-8'))
else:
    print(f"请求失败，状态码: {response.status_code}")
