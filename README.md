
# Reptile

#### 介绍

基于当前项目所做的接口自动化框架，学习到了新的封装技术及思路

#### 软件架构

pytest + allure + nb_log + jenkins 

#### 安装教程

开箱即用

#### 使用说明

1. 修改Data目录下的配置
2. 修改sink/source端的连接器名称即可
3. 如果在不同域名下，则需要修改urls中的域名信息
4. 需要执行测试用例的时候可以采用命令行方式执行

### 1. 想要创建不同的Source
#### 1.1 创建oracle源
进入到testcase/config/all_parma.yaml文件，修改create_type为oracle，若想创建其他的则填写不同的类型
1.oracle
2.dm
3.vb
4.pg
5.mysql

#### 1.2 创建connect赋予名称
进入到business/smartsync/create_connect.py文件
修改main下的文件名称


**CreateSync().CreateSource(token, '替换你想生成的名称即可').json()**

### 2. 想要创建不同的Sink
#### 2.1 创建oracle目标

同上

#### 2.2 创建connect赋予名称
进入到business/smartsync/create_connect.py文件
修改main下的文件名称

**注意：这里是sink哦  CreateSync().CreateSink(token, '替换你想生成的名称').json()**


### 3. 想要批量执行测试用例


这里需要注意的是需要将文件中的url全部替换成，当前代码因为调试我注销掉了这些内容
***super().__init__(host=settings.api_host)***


**执行单个测试用例：pytest Sample/test_login.py --settings=Config.test_config**

这里会将你的配置文件加载，相当于直接调用 settings.api_host 等同于 Config.test_config.api_host


#### case执行命令：

执行所有：

- window：py.test --alluredir=./test_report testcase/ --settings=config.cht-test-1
- Mac：pytest --alluredir=./test_report testcase/ --settings=config.cht-test-1
- 列出最慢top10的case：pytest --alluredir=./test_report testcase/ --settings=config.cht-test-1 --durations=10
- 报告展示执行过程中的变量信息：pytest testcase -l --settings=config.cht-test-1
- 执行单个case： py.test --alluredir=./test_report testcase/http/kyc/test_kyc_info.py --settings=config.cht-test-1
- 执行单个方法接口： pytest Testcase/Login/test_login.py::TestDeposit::test_get_coin_address --settings=config.cht-test-1

启动allure
allure serve ./test_report
