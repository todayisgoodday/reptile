# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/9/7 16:59
# File      : test_config.py
# explain   : 文件说明


api_host = "192.168.3.127:8081"

create_colony = "cluster/add"  # 新增集群分组
get_token_url = "auth/captcha?_t=1692003021098"  # 获取验证码信息
url = "gssConfigList/submitList"  # 创建服务
