# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/9/19 14:30
# File      : diff.py
# explain   : 文件说明

from common.log import logger

class AssertInfo:
    data = []


def diff_json(response_data, assert_data):
    """
    Compare the JSON data format
    """
    if isinstance(response_data, dict) and isinstance(assert_data, dict):
        response_keys = set(response_data.keys())
        assert_keys = set(assert_data.keys())

        # Check missing keys in response_data
        missing_response_keys = assert_keys - response_keys

        for key in missing_response_keys:
            info = f"❌ Response data has no key: {key}"
            logger.info(info)
            AssertInfo.data.append(info)

        # Recursively check keys present in both data
        for key in response_keys.intersection(assert_keys):
            diff_json(response_data[key], assert_data[key])

    elif isinstance(response_data, list) and isinstance(assert_data, list):
        if len(response_data) != len(assert_data):
            logger.error(f"list len: '{len(response_data)}' != '{len(assert_data)}'")
            info = f"❌ list len: '{len(response_data)}' != '{len(assert_data)}'"
            AssertInfo.data.append(info)
            return

        # Recursively check list elements
        for src_list, dst_list in zip(response_data, assert_data):
            diff_json(src_list, dst_list)

    elif response_data != assert_data:
        info = f"❌ Value are not equal: {response_data}"
        logger.error(info)
        AssertInfo.data.append(info)
