# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/9/7 17:37
# File      : login.py
# explain   : 文件说明


import pytest
from common.read_yaml import get_yml_data
from business.smartsync.login import Login
from common.assetcase import TestCase
from common.log import logger


class TestLogin(TestCase):

    @classmethod
    def setup_class(cls):
        cls.action = Login()

    @pytest.mark.parametrize('case_data', get_yml_data('sync', 'login', 'login_params'))
    def test_login_success(self, case_data):
        """
        登录成功
        :param case_data:
        :return:
        """
        self.assertJson(case_data.get('verify'), self.action.login(case_data['palayod']).json()), '登录失败，测试用例报错......'


if __name__ == '__main__':
    pytest.main(['-s', '-v', 'test_login.py'])
