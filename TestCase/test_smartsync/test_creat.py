# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/9/25 10:21
# File      : test_creat.py
# explain   : 文件说明


import pytest
from business.smartsync.create_connect import CreateSync
from common.read_yaml import get_yml_data
from common.assetcase import TestCase
from common.help_user import GetToken


class TestCreat(TestCase):

    def setup_class(cls):
        cls.cre = CreateSync()
        cls.token = GetToken()

    def test_creat(self, name=None):
        self.cre.CreateSource(self.token, name)


if __name__ == '__main__':
    pytest.main(['-s', '-v', 'test_creat.py'])
