# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/12/13 17:24
# File      : test_create_migrate.py
# explain   : 文件说明


import pytest
from business.migrate.create_task import CreateTask
from common.migrate_source import Replace
from common.assetcase import TestCase


class TestCreat(TestCase):

    def setup_class(cls):
        cls.cre = CreateTask()
        cls.rpl = Replace()

    def test_creat_migrate(self):
        self.cre.create_task(self.rpl.replace())


if __name__ == '__main__':
    pytest.main(['-s', '-v', 'test_create_migrate.py'])
